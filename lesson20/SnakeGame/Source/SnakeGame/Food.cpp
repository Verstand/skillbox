// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	//this->SetActorHiddenInGame(true);
	//MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AFood::HandeleBeginOverlap);
	//MeshComponent->OnComponentEndOverlap.AddDynamic(this, &AFood::HandeleEndOverlap);
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::HandeleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	this->ChangeLocation();
}

void AFood::HandeleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	this->SetActorHiddenInGame(false);
}

void AFood::ChangeLocation() {
	int x = rand() % 600 - 300, y = rand() % 600 - 300;
	FVector NewLocation(x, y, 26);
	this->SetActorLocation(NewLocation);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->AddSnakeElement();
			Snake->AddFood();
			this->Destroy();
		}
	}
} 

void AFood::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision) {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}


