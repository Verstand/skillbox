// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSet.h"

// Sets default values
AFoodSet::AFoodSet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodSet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

