// lesson18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <assert.h>

using namespace std;

template <class T>

class Stack {
private:
    T* stack;
    int size, index;
public:
    stack() {
        size = 0;
        index = 0;
    }
    void push(T val) {
        if (index == size - 1) {
            T* newStack = new T[size * 2];
            for (int i = 0; i < size; ++i) {
                newStack[i] = stack[i];
            }
            delete[] stack;
            stack = newStack;
            size += size;
         }
        stack[index] = val;
        index++;
    }
    T pop() {
        if (index == 0) {
            cout << "incorrect operation\n";
            assert(0);
        }
        index--;
        return stack[index];
    }

};

Stack<double> st;

int main()
{
    st.push(5.5);
    cout << st.pop() << endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
